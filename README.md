####  Deploy to the remote server with Bitbucket Pipes

This repo contains a basic example for deploying applications to the remote server using [scp-deploy](https://bitbucket.org/atlassian/scp-deploy/src/master/) pipe.
It this example we test, build and deploy a simple ReactJS app created with [create-react-app](https://github.com/facebook/create-react-app) to the remote server.

#### Hot wo use thie example

##### Forking the repo

Create a fork of this repo, see [Forking a Repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).

##### Enabling Pipelines in your repo 

To enable Piplines go to *Project*  > *Settings* > *Pipelines* and toggle the **Enable Pipelines** button. This can also be done from the **Pipelines** tab in your repository sidebar when you enable Pipelines for the first time. See also [Piplines Getting started](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html) for more instructions.

##### Configuring pipe variables

To use this example you'll need to configure `USER`, `SERVER`, `REMOTE_PATH` and `LOCAL_PATH ` pipelines variables in your repository. 

See **User-defined variables** section in [Variables in pipelines](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html) for how to configure Pipelines variables. 

##### Configuring your SSH keys

Set up an [SSH key in Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html) or provide them as a variable `SSH_KEY`.

By default, the SCP-deploy pipe will automatically use your configured SSH key and known_hosts file configured from the [Bitbucket Pipelines administration pages](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html). You can pass the `SSH_KEY` parameter to use an alternate SSH key as per the instructions in the docs for [using multiple ssh keys](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-multiple_keys).


#### Verifying the results of the pipe run

After you've enabled Pipelines and configured all variables you can navigate to the Pipelines section of your fork and explore the current status of the builds. If the build is :successful: you should be able to navigate to `<your-project-name> URL` where your simple rating service should be up and running.
